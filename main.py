#!/usr/bin/env python
"""
This contains four functions to show how addition, subject, multiplication
and division is done in Python 2 or 3.

Created by: David Tran

"""


from pymongo import MongoClient


def addition(term_a, term_b):
    """
    This adds two terms together and returns the result.

    Arguments:
        term_a:   (any number) One of two numbers that will be added.

        term_b:   (any number) One of two numbers that will be added.


    Returns:
        The resultant sum (any number). 
        
    Notes:
        Due to the nature of operators in Python, the "addition" operator is 
        overloaded so other values or data structures can be added
        together without error and have a valid result.

    Exception:
        TypeError: For any invalid additions, this will be thrown.

    """


    return term_a + term_b


def subtraction(minuend, subtrahend):
    """
    This subtracts the subtrahend from the minuend and returns the difference.

    Arguments:
        minuend:   (any number) The number we initially have to subtract from.

        subtrahend:   (any number) The number we are subtracting.


    Returns:
        The resultant difference (any number). 

    Notes:
        Due to the nature of operators in Python, the "difference" operator is 
        overloaded so other values or data structures can be subtracted without
        error and have a valid result.

    Exception:
        TypeError: For any invalid subtractions, this will be thrown.

    """


    return minuend - subtrahend


def multiplication(factor_a, factor_b):
    """
    This multiplies two factors together and returns the product.

    Arguments:
        factor_a:   (any number) One of two numbers that will be multiplied.

        factor_b:   (any number) One of two numbers that will be multiplied.


    Returns:
        The resultant product (any number). 

    Notes:
        Due to the nature of operators in Python, the "multiplication" operator 
        is overloaded so other values or data structures can be multiplied 
        without error and have a valid result.

    Exception:
        TypeError: For any invalid multiplications, this will be thrown.

    """


    return factor_a * factor_b


def division(dividend, divisor):
    """
    This divides the dividend from the divisor and returns the quotient.

    Arguments:
        dividend:   (any number) The numerator or number we start with.

        divisor:   (any number) The denominator or number we are dividing with.


    Returns:
        The resultant quotient (any number). 

    Notes:
        Due to the nature of operators in Python, the "division" operator is 
        overloaded so other values or data structures can be divided without
        error and have a valid result.

    Warning:
        Due to the different versions of Python, division (/) can have 
        different meanings between 2 and 3. This can be fixed with an import
        or just have two different tests to show off how the results can 
        differ. See the test of this function for more information.

        Reference: https://www.python.org/dev/peps/pep-0238/

    Exceptions:
        TypeError: For any invalid division, this will be thrown.

        ZeroDivisionError: If the divisor is zero, this will be thrown.

    """


    return dividend / divisor


def database_connect(host="127.0.0.1", port_number=27017):
    """
    This connects to a MongoDB.

    Arguments:
        host:   (string) This is a URI or IP address of the DB in question.
          The default is on the local machine.

        port_number   (int) The port number of the database. The default is
          27017.

    Returns:
    A valid MongoClient.

    Exceptions:
        pymongo.errors.ConnectionFailure for a bad connection

    Reference: https://api.mongodb.com/python/current/tutorial.html

    """


    return MongoClient(host=host, port=port_number)


